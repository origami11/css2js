﻿
var jsp = require("./jsparse");
var fs = require("fs");

var rtoken = jsp.rtoken;
var choice = jsp.choice;
var wseq = jsp.wsequence;
var seq = jsp.sequence;
var rep0 = jsp.repeat0;
var rep1 = jsp.repeat1;
var opt = jsp.optional;
var action = jsp.action;
var list = jsp.wlist;
var end_p = jsp.end_p;


var simple = function (state) { return simple(state); };

var ident = rtoken('[A-Za-z\-][\-\\w\\d_]*', '<ident>');

var number = action(rtoken('[+-]?\\d+(\\.\\d*)?', '<number>'), function (r) {     
    return parseFloat(r); 
});

var fn = action(wseq(ident, "(", simple, ")"), function (r) {
    return {"type": "call", "fn": r[0], "args": r[2]};
});

var rawnumber = action(number, function (r) {
    return {"type": "number", "n": r};
});

var percent = action(seq(number, "%"), function (r) {
    return {"type": "percent", "n": r[0]};
});

var degs = action(seq(number, "deg"), function (r) {
    return {"type": "deg", "n": r[0]};
});

var pixels = action(seq(number, "px"), function (r) {
    return {"type": "pixel", "n": r[0]};
});

var simple = choice(pixels, degs, percent, rawnumber);
var value = choice('none', simple, fn);

var prop = action(wseq(ident, ":", rep1(value), ";"), function (r) {
    return {"key": r[0], "value": r[2]};
});
var state = action(wseq(percent, "{", rep0(prop), "}"), function (r) {
    return {"offset": r[0], "names": r[2]};
});
var keyframes = action(wseq("@keyframes", ident, "{", rep0(state), "}"), function (r) {
    return {"name": r[1], "states": r[3]};        
});

var expression = wseq(rep0(keyframes), end_p);;

function parseExpression(data, pos) {
    return expression(new jsp.ParseState(data, pos));
}

function getStateValues(state) {
    var vid = 0;
    var values = {};
    for(var i = 0; i < state.length; i++) {
        var item = state[i];
        for(var k = 0; k < item.value.length; k++) {
            var value = item.value[k];
            if (value.type == 'call') {            
                values[item.key + '_' + k + '_' + value.fn] = value.args.n;
            } else if (item.value.length > 1) {
                values[item.key + '_' + k] = value.n;
            } else {
                values[item.key] = value.n;
            }
        }                        
    }
    return values;
}

function suffix(x) {
    if (x == 'pixel') return 'px';
    if (x == 'deg') return 'deg';
    if (x == 'percent') return '%';
    return '';
} 

function makePropValues(key, values) {
    var result = [], name;
    for(var k = 0; k < values.length; k++) {
        var value = values[k];
        if (value.type == 'call') {            
            name = key + '_' + k + '_' + value.fn;
            result.push("'" + value.fn + "(' + this." + name + " + '" + suffix(value.args.type) + ")'");
        } else if (value.length > 1) {
            name = key + '_' + k;
            result.push("this." + name + " + '" + suffix(value.type) + "'");
        } else {
            name = key;
            result.push("this." + name + " + '" + suffix(value.type) + "'");
        }
    }               
    return result.join(" + ' ' + ");
}

function makeStyleFn(state) {
    var values = [];
    for(var i = 0; i < state.length; i++) {
        var item = state[i];
        
        values.push("var s" + i + " = " + makePropValues(item.key, item.value) + ";");
        values.push("obj.style." + item.key + " = " + 's' + i + ";");
        if (item.key == 'transform') {
            values.push("obj.style.msTransform" + item.key + " = " + 's' + i + ";");
            values.push("obj.style.webkitTransform" + item.key + " = " + 's' + i + ";");
        }
    }
    return values.join("\r\n        ");
}

function makeSteps(states) {
    var result = [];
    for(var i = 0; i < states.length - 1; i ++) {
        var step = [
        "    var t" + i + " = new TWEEN.Tween(s"+i+ ").to(s"+(i+1)+", time * " + (states[i+1].offset.n - states[i].offset.n)/100+").onUpdate(function () {",       
        "        " + makeStyleFn(states[i+1].names),
        "    }).easing(TWEEN.Easing.Linear.None);",
        ].join("\r\n");
        result.push(step);
    }

    return result.join("\r\n");
}

function makeChains(states) {
    var result = [];
    for(var i = 1; i < states.length - 1; i++) {
        var step = ["    t" + (i-1) +".chain(t" + i + ");"].join("\r\n");
        result.push(step);
    }
    return result.join("\r\n");
}

var data = fs.readFileSync('animation.css', 'utf-8');
var expr = parseExpression(data, 0);

if (expr.result) {
    function makeAnimationFn(ast) {
        var states = [], state_var = [];
        for(var i = 0; i < ast.states.length; i++) {
            states.push(getStateValues(ast.states[i].names));
        }
        for(var i = 0; i < states.length; i++) {
            state_var.push("var s" + i + " = " + JSON.stringify(states[i]) + ";");
        }

        var fn = [  
        "animation." + ast.name + " = function (obj, time) {",
        "    " + state_var.join("\r\n    "),
        makeSteps(ast.states),        
        makeChains(ast.states),
        "    t0.start();",
        "};"
        ];
        return fn.join("\r\n");
    }
    //console.log(JSON.stringify(expr.ast, null, 4));

    function makeAnimations(list) {
        return "var animation = {};\r\n" + list.map(makeAnimationFn).join("\r\n");
    }

    fs.writeFileSync('animation.js', makeAnimations(expr.ast[0]));
} else {
    console.log(expr.error);
}