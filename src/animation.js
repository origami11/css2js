var animation = {};
animation.fadeInMoveLeft = function (obj, time) {
    var s0 = {"opacity":0,"transform_0_translateX":100};
    var s1 = {"opacity":1,"transform_0_translateX":0};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = this.opacity + '';
        obj.style.opacity = s0;
        var s1 = 'translateX(' + this.transform_0_translateX + '%)';
        obj.style.transform = s1;
        obj.style.msTransformtransform = s1;
        obj.style.webkitTransformtransform = s1;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.fadeOutMoveRight = function (obj, time) {
    var s0 = {"opacity":1,"transform_0_translateX":0};
    var s1 = {"opacity":0,"transform_0_translateX":100};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = this.opacity + '';
        obj.style.opacity = s0;
        var s1 = 'translateX(' + this.transform_0_translateX + '%)';
        obj.style.transform = s1;
        obj.style.msTransformtransform = s1;
        obj.style.webkitTransformtransform = s1;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.fadeIn = function (obj, time) {
    var s0 = {"opacity":0};
    var s1 = {"opacity":1};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = this.opacity + '';
        obj.style.opacity = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.setCursorOnFirstPosition = function (obj, time) {
    var s0 = {"transform_0_translateX":-2};
    var s1 = {"transform_0_translateX":-34};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = 'translateX(' + this.transform_0_translateX + '%)';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.setCursorOnFirstPosition2 = function (obj, time) {
    var s0 = {"transform_0_translateX":29};
    var s1 = {"transform_0_translateX":-34};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = 'translateX(' + this.transform_0_translateX + '%)';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.setCursorOnSecondPosition = function (obj, time) {
    var s0 = {"transform_0_translateX":-34};
    var s1 = {"transform_0_translateX":-2};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = 'translateX(' + this.transform_0_translateX + '%)';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.setCursorOnSecondPosition2 = function (obj, time) {
    var s0 = {"transform_0_translateX":29};
    var s1 = {"transform_0_translateX":-2};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = 'translateX(' + this.transform_0_translateX + '%)';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.setCursorOnThirdPosition = function (obj, time) {
    var s0 = {"transform_0_translateX":-34};
    var s1 = {"transform_0_translateX":29};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = 'translateX(' + this.transform_0_translateX + '%)';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.setCursorOnThirdPosition2 = function (obj, time) {
    var s0 = {"transform_0_translateX":-2};
    var s1 = {"transform_0_translateX":29};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = 'translateX(' + this.transform_0_translateX + '%)';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.setFirstSlideActive = function (obj, time) {
    var s0 = {"transform_0_translateX":-103};
    var s1 = {"transform_0_translateX":3};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = 'translateX(' + this.transform_0_translateX + '%)';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.setSecondSlideActive = function (obj, time) {
    var s0 = {"transform_0_translateX":-103};
    var s1 = {"transform_0_translateX":3};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = 'translateX(' + this.transform_0_translateX + '%)';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.setSecondSlideActive2 = function (obj, time) {
    var s0 = {"transform_0_translateX":103};
    var s1 = {"transform_0_translateX":3};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = 'translateX(' + this.transform_0_translateX + '%)';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.setThirdSlideActive = function (obj, time) {
    var s0 = {"transform_0_translateX":103};
    var s1 = {"transform_0_translateX":3};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = 'translateX(' + this.transform_0_translateX + '%)';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.setSlideInactiveLeft = function (obj, time) {
    var s0 = {"transform_0_translateX":3};
    var s1 = {"transform_0_translateX":-130};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = 'translateX(' + this.transform_0_translateX + '%)';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.setSlideInactiveRight = function (obj, time) {
    var s0 = {"transform_0_translateX":3};
    var s1 = {"transform_0_translateX":130};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = 'translateX(' + this.transform_0_translateX + '%)';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.flee = function (obj, time) {
    var s0 = {"opacity":1};
    var s1 = {"opacity":0};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = this.opacity + '';
        obj.style.opacity = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.heartRotateRight = function (obj, time) {
    var s0 = {"transform_0_rotate":0};
    var s1 = {"transform_0_rotate":360};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = 'rotate(' + this.transform_0_rotate + 'deg)';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.heartScale = function (obj, time) {
    var s0 = {"transform_0_scale":1};
    var s1 = {"transform_0_scale":1.5};
    var s2 = {"transform_0_scale":1};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 0.5).onUpdate(function () {
        var s0 = 'scale(' + this.transform_0_scale + ')';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);
    var t1 = new TWEEN.Tween(s1).to(s2, time * 0.5).onUpdate(function () {
        var s0 = 'scale(' + this.transform_0_scale + ')';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
    }).easing(TWEEN.Easing.Linear.None);
    t0.chain(t1);
    t0.start();
};
animation.heartFastOpacity = function (obj, time) {
    var s0 = {"opacity":0};
    var s1 = {"opacity":0.4};
    var s2 = {"opacity":0.7};
    var s3 = {"opacity":1};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 0.05).onUpdate(function () {
        var s0 = this.opacity + '';
        obj.style.opacity = s0;
    }).easing(TWEEN.Easing.Linear.None);
    var t1 = new TWEEN.Tween(s1).to(s2, time * 0.1).onUpdate(function () {
        var s0 = this.opacity + '';
        obj.style.opacity = s0;
    }).easing(TWEEN.Easing.Linear.None);
    var t2 = new TWEEN.Tween(s2).to(s3, time * 0.85).onUpdate(function () {
        var s0 = this.opacity + '';
        obj.style.opacity = s0;
    }).easing(TWEEN.Easing.Linear.None);
    t0.chain(t1);
    t1.chain(t2);
    t0.start();
};
animation.heartOpacity = function (obj, time) {
    var s0 = {"opacity":0};
    var s1 = {"opacity":1};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 1).onUpdate(function () {
        var s0 = this.opacity + '';
        obj.style.opacity = s0;
    }).easing(TWEEN.Easing.Linear.None);

    t0.start();
};
animation.myOrbit = function (obj, time) {
    var s0 = {"transform_0_rotate":0,"transform_1_translateX":0,"transform_2_rotate":0,"opacity":0};
    var s1 = {"opacity":0.4};
    var s2 = {"opacity":0.7};
    var s3 = {"transform_0_rotate":360,"transform_1_translateX":60,"transform_2_rotate":-360,"opacity":1};
    var t0 = new TWEEN.Tween(s0).to(s1, time * 0.05).onUpdate(function () {
        var s0 = this.opacity + '';
        obj.style.opacity = s0;
    }).easing(TWEEN.Easing.Linear.None);
    var t1 = new TWEEN.Tween(s1).to(s2, time * 0.1).onUpdate(function () {
        var s0 = this.opacity + '';
        obj.style.opacity = s0;
    }).easing(TWEEN.Easing.Linear.None);
    var t2 = new TWEEN.Tween(s2).to(s3, time * 0.85).onUpdate(function () {
        var s0 = 'rotate(' + this.transform_0_rotate + 'deg)' + ' ' + 'translateX(' + this.transform_1_translateX + 'px)' + ' ' + 'rotate(' + this.transform_2_rotate + 'deg)';
        obj.style.transform = s0;
        obj.style.msTransformtransform = s0;
        obj.style.webkitTransformtransform = s0;
        var s1 = this.opacity + '';
        obj.style.opacity = s1;
    }).easing(TWEEN.Easing.Linear.None);
    t0.chain(t1);
    t1.chain(t2);
    t0.start();
};