﻿(function () {

    function setAnimation(obj, name, time) {
        animation[name](document.querySelector(obj), time * 1000);
    }

//    setAnimation('#rect', 'fadeInMoveLeft', 1);
//    setAnimation('#rect', 'fadeOutMoveRight', 1);
//    setAnimation('#rect', 'myOrbit', 2);
    setAnimation('#rect', 'heartScale', 1);


    function animate(time) {
        requestAnimationFrame(animate);
        TWEEN.update(time);
    }

    animate(0);
} ());